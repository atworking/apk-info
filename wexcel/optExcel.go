/**
* @Date:    2021/4/21 14:33
* @Author:  韩星星
* @Description: 写入Excel
 */

package wexcel

import (
	"github.com/360EntSecGroup-Skylar/excelize"
	"log"
	"strconv"
)

var sname = "日常研判" //Sheet名字

// 创建Excel文件
func CrateExcel(name string) (err error){

	f := excelize.NewFile()
	// 工作表
	index := f.NewSheet(sname)
	// 设置单元格的值
	f.SetCellValue(sname, "A1", "序号")
	f.SetCellValue(sname, "B1", "时间")
	f.SetCellValue(sname, "C1", "文件名")
	f.SetCellValue(sname, "D1", "文件MD5")
	f.SetCellValue(sname, "E1", "文件大小")
	f.SetCellValue(sname, "F1", "应用名称")
	f.SetCellValue(sname, "G1", "包名")
	f.SetCellValue(sname, "H1", "版本号")
	f.SetCellValue(sname, "I1", "TargetSDK")
	f.SetCellValue(sname, "J1", "签名Issuer")
	f.SetCellValue(sname, "K1", "签名Subjcet")
	f.SetCellValue(sname, "L1", "签名MD5")
	f.SetCellValue(sname, "M1", "签名SHA1")
	f.SetCellValue(sname, "N1", "签名序列号")
	f.SetCellValue(sname, "O1", "TX名")
	f.SetCellValue(sname, "P1", "TX描述")
	f.SetCellValue(sname, "Q1", "Qihoo")
	f.SetCellValue(sname, "R1", "Baidu")
	f.SetCellValue(sname, "S1", "Rising")
	f.SetCellValue(sname, "T1", "Kaspersky")
	f.SetCellValue(sname, "U1", "Alibaba")
	f.SetCellValue(sname, "V1", "Kingsoft")
	f.SetCellValue(sname, "W1", "Tencent")
	f.SetCellValue(sname, "X1", "ESET-NOD32")
	f.SetCellValue(sname, "Y1", "JiangMin")
	f.SetCellValue(sname, "Z1", "Virus/Total")

	f.SetActiveSheet(index) //设置默认工作表

	if err = f.SaveAs(name); err != nil {
		log.Fatalf("初始化Excel失败:%s",err)
		return
	}
	return
}

// 读取文档写入
func WriteExcel(excel string, row, index int, time, filename, apkmd5, apkname, pkg, version, issuer, subject, sigmd5, sigsha1, sigsn, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtResult string, size int64, target int32) {
	f, err := excelize.OpenFile(excel)
	if err != nil {
		log.Fatalf("打开Excel失败")
		return
	}
	f.SetSheetRow(sname, "A"+strconv.Itoa(row), &[]interface{}{index, time, filename, apkmd5, size, apkname, pkg, version, target, issuer, subject, sigmd5, sigsha1, sigsn, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtResult}) //按行赋值
	// 插入值
	f.Save()
}
