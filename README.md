#  获取apk信息
## 使用库
• github.com/Unknwon/goconfig

获取ini配置文件，如果不需要VT的用户配置可以不需要

• github.com/shogo82148/androidbinary

解析APK文件需要主要解析清单文件内容，但是不包括签名，支持签名文件需要修改

• github.com/360EntSecGroup-Skylar/excelize

输出到Excel库

• go.mozilla.org/pkcs7

解析安卓签名所需要的库，但是只支持验证(发行者和序列号对应)，如果是修改后的不能获取 需要修改自定义

## sign
此部分参考 `go.mozilla.org/pkcs7` 从定义的获取APK签名，因为如果使用原始库 验证未通过的签名获取不到
如`1.RSA`

## 获取签名文件
对库`github.com/shogo82148/androidbinary` 源码增加一个公开获取签名文件的方法
```go
func (k *Apk) ReadSignerFile() (data []byte, err error) {
    buf := bytes.NewBuffer(nil)
    for _, file := range k.zipreader.File {
        if !strings.Contains(file.Name,"META-INF/"){
            continue
        }
        if !strings.HasSuffix(file.Name, ".RSA") && !strings.HasSuffix(file.Name,".DSA"){
            continue
        }
        rc, er := file.Open()
        if er != nil {
            err = er
            return
        }
        defer rc.Close()
        _, err = io.Copy(buf, rc)
        if err != nil {
            return
        }
        return buf.Bytes(), nil
    }
    return nil, fmt.Errorf("File %s not found", strconv.Quote("签名文件"))
}
```
## wexcel
用于创建了excel
## ret
主要用于返回结果
## network
用于TX和VT在线查杀 根据MD5
## utils
解析APK获取信息和文件信息
## lib
github.com/shogo82148/androidbinary v1.2代码魔改本地化

## 更新说明
修复了错误签名无法解析崩溃推出：2021-05-27