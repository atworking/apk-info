/**
* @Date:    2021/4/20 15:11
* @Author:  韩星星
* @Description: main函数
 */

package main

import (
	"ApkInfo/ret"
	"ApkInfo/wexcel"
	"flag"
	"log"
	"time"
)

func main() {
	var path string
	var tx, vt int
	log.Println("当前版本：v2.2.2")
	flag.StringVar(&path, "p", "", "文件/文件夹路径：-p xxx")
	flag.IntVar(&tx, "tx", 0, "是否启用腾讯在线查杀 ：-tx 1")
	flag.IntVar(&vt, "vt", 0, "是否启用Visual查杀 : -vt 1")
	flag.Parse() 	// 解析命名放参数
	if len(path) == 0 || path == "" {
		log.Fatalf("请输入路径：-p xxx")
		return
	}
	log.Println("程序开始运行...")
	now := time.Now()
	excelfile := now.Format("2006-01-02-15-04-05") + ".xlsx"
	err := wexcel.CrateExcel(excelfile) //创建Excel
	if err != nil {
		log.Fatalf("创建[%s]excel文件失败:%v", excelfile, err)
		return
	}
	/*
		// 第一种直接写测试
		err = ret.OptToExcel("F:\\迅雷下载", excelfile, 1, 0)
		if err!=nil{
			log.Fatalln("写入excel失败:",err)
			return
		}
	*/
	// 第二种写到结构体测试
	err = ret.OptStructToExcet(path, excelfile, tx, vt)
	if err != nil {
		log.Fatalln(err)
		return
	}
	end := time.Since(now)
	log.Println("程序运行结束...")
	log.Println("共耗时：", end)
}
