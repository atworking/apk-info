/**
* @Date:    2021/4/21 11:59
* @Author:  韩星星
* @Description: 将结果写入excel 两种返回 第一种字节写 第二种通过返回结构体写
 */

package ret

import (
	"github.com/360EntSecGroup-Skylar/excelize"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"time"
)

// 第一种方法直接写
// 操作
func OptToExcel(path, excelName string, tx, vt int) (err error) {
	tag, err := isFile(path)
	if err != nil {
		return
	}
	eTime := time.Now().Format("2006/01/02 15:04:05")
	ef, err := excelize.OpenFile(excelName) //打开Excel
	if err != nil {
		log.Fatalf("打开Excel失败")
		return
	}
	if tag { //单个文件处理
		log.Println("单个文件")
		fileToExcel(path, eTime, ef, tx, vt)
		return nil
	}
	/* 文件夹处理 */
	err = dirToExcel(path, eTime, ef, tx, vt)
	if err != nil {
		return err
	}
	return nil
}

// 单个文件
func fileToExcel(path, eTime string, excelFile *excelize.File, tx, vt int) {
	apkMd5, apkName, pkg, version, issuer, subject, signMd5, signSha1, serialNumber, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet, size, target := GetResult(path, tx, vt)
	excelFile.SetSheetRow("日常研判", "A"+strconv.Itoa(2), &[]interface{}{1, eTime, path, apkMd5, size, apkName, pkg, version, target, issuer, subject, signMd5, signSha1, serialNumber, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet})
	excelFile.Save()
	return
}

func dirToExcel(path, eTime string, excelFile *excelize.File, tx, vt int) (err error) {
	readDir, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatalf("获取目录[%s]目录错误", path)
		return
	}
	for i, f := range readDir {
		if f.IsDir() {
			dirToExcel(path+"\\"+f.Name(), eTime, excelFile, tx, vt)
		} else {
			log.Printf("[%s]计算中...", f.Name())
			apkMd5, apkName, pkg, version, issuer, subject, signMd5, signSha1, serialNumber, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet, size, target := GetResult(path+"\\"+f.Name(), tx, vt)
			excelFile.SetSheetRow("日常研判", "A"+strconv.Itoa(i+2), &[]interface{}{i + 1, eTime, f.Name(), apkMd5, size, apkName, pkg, version, target, issuer, subject, signMd5, signSha1, serialNumber, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet})
		}
	}
	excelFile.Save() //保存
	return
}

// 第二种写道结构体通过返回值写

type ApkResult struct {
	Id int // 序号
	FName         string // 文件名
	FMd5          string // apkMD5
	FSize         int64  // 文件大小
	AName         string // apk应用名
	APkg          string // apk包名
	AVersion      string // apk版本号
	ATarget       int32  // apkTargetSDK
	AIssuer       string // APK 签名 issuer
	ASubject      string // apk 签名 subject
	ASignMd5      string // apk签名md5
	ASignSha1     string // APK签名sha1
	ASerialNumber string // APK签名序列号
	TxName        string // 腾讯引擎结果
	TxDesc        string // 腾讯引擎描述
	VQihoo        string // VT 360
	VBaidu        string // VT 百度
	VRising       string // VT rising
	VKaspersky    string // VT Kaspersky
	VAlibaba      string // vt alibaba
	VKingsoft     string // vt 金山
	VTx           string // vt 腾讯
	VEset         string // vt ESET-NOD32
	VJm           string // VT 江民
	VRet          string // VT 杀毒引擎结果 病毒/总数
}

// 从结构体获取所有文件

func OptStructToExcet(path, excelName string, tx, vt int) (err error) {
	tag, err := isFile(path)
	if err != nil {
		return
	}
	ef, err := excelize.OpenFile(excelName) //打开Excel
	eTime := time.Now().Format("2006/01/02 15:04:05")
	if err != nil {
		log.Fatalf("打开Excel失败")
		return
	}
	if tag {
		log.Println("单个文件")
		ret := fileToStruct(path, tx, vt)

		ef.SetSheetRow("日常研判", "A2", &[]interface{}{ret.Id, eTime, ret.FName, ret.FSize, ret.FMd5, ret.AName, ret.APkg, ret.AVersion, ret.ATarget, ret.AIssuer, ret.ASubject, ret.ASignMd5, ret.ASignSha1, ret.ASerialNumber, ret.TxName, ret.TxDesc, ret.VQihoo, ret.VBaidu, ret.VRising, ret.VKaspersky, ret.VAlibaba, ret.VKingsoft, ret.VTx, ret.VEset, ret.VJm, ret.VRet})
		ef.Save() //保存文件
		return
	}
	rets, err := dirStructToExcel(path, tx, vt)
	if err != nil {
		log.Fatalln("获取文件结构体信息失败:", err)
		return
	}
	for i, v := range rets {
		ef.SetSheetRow("日常研判", "A"+strconv.Itoa(i+2), &[]interface{}{v.Id, eTime, v.FName, v.FMd5, v.FSize, v.AName, v.APkg, v.AVersion, v.ATarget, v.AIssuer, v.ASubject, v.ASignMd5, v.ASignSha1, v.ASerialNumber, v.TxName, v.TxDesc, v.VQihoo, v.VBaidu, v.VRising, v.VKaspersky, v.VAlibaba, v.VKingsoft, v.VTx, v.VEset, v.VJm, v.VRet})
	}
	ef.Save() //保存文件
	return
}

// 单个文件返回结构体
func fileToStruct(path string, tx, vt int) (apkResults *ApkResult) {
	apkMd5, apkName, pkg, version, issuer, subject, signMd5, signSha1, serialNumber, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet, size, target := GetResult(path, tx, vt)
	return &ApkResult{
		Id:            1,
		FName:         path,
		FMd5:          apkMd5,
		FSize:         size,
		AName:         apkName,
		APkg:          pkg,
		AVersion:      version,
		ATarget:       target,
		AIssuer:       issuer,
		ASubject:      subject,
		ASerialNumber: serialNumber,
		ASignMd5:      signMd5,
		ASignSha1:     signSha1,
		TxName:        xcheck,
		TxDesc:        desc,
		VQihoo:        qihoo,
		VBaidu:        baidu,
		VRising:       rising,
		VKaspersky:    kaspers,
		VAlibaba:      alibaba,
		VKingsoft:     kingsoft,
		VTx:           tencent,
		VEset:         esetnod,
		VJm:           jiangmin,
		VRet:          vtRet,
	}
}

// 目录返回结构体
func dirStructToExcel(path string, tx, vt int) (apkResults []*ApkResult, err error) {
	readDir, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatalf("获取目录[%s]目录错误", path)
		return
	}
	for i, f := range readDir {
		if f.IsDir() {
			dirStructToExcel(path+"\\"+f.Name(), tx, vt)
		} else {
			log.Printf("[%s]计算中...", f.Name())
			apkMd5, apkName, pkg, version, issuer, subject, signMd5, signSha1, serialNumber, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet, size, target := GetResult(path+"\\"+f.Name(), tx, vt)
			tmp := &ApkResult{
				Id:            i + 1,
				FName:         f.Name(),
				FMd5:          apkMd5,
				FSize:         size,
				AName:         apkName,
				APkg:          pkg,
				AVersion:      version,
				ATarget:       target,
				AIssuer:       issuer,
				ASubject:      subject,
				ASerialNumber: serialNumber,
				ASignMd5:      signMd5,
				ASignSha1:     signSha1,
				TxName:        xcheck,
				TxDesc:        desc,
				VQihoo:        qihoo,
				VBaidu:        baidu,
				VRising:       rising,
				VKaspersky:    kaspers,
				VAlibaba:      alibaba,
				VKingsoft:     kingsoft,
				VTx:           tencent,
				VEset:         esetnod,
				VJm:           jiangmin,
				VRet:          vtRet,
			}
			apkResults = append(apkResults, tmp)
		}
	}
	return
}

// 判断传入的时文件
func isFile(file string) (bool, error) {
	stat, err := os.Stat(file)
	if err != nil {
		return false, err
	}
	return !stat.IsDir(), nil
}
