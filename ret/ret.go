/**
* @Date:    2021/4/21 12:38
* @Author:  韩星星
* @Description: 获取最终结果
 */

package ret

import (
	"ApkInfo/network"
	"ApkInfo/utils"
	"log"
	"math/rand"
	"strings"
	"time"
)

// 返回结果
func GetResult(file string,tx, vt int) (apkMd5, apkName, pkg, version, issuer, subject, signMd5, signSha1, serialNumber, xcheck, desc, qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet string, size int64, target int32) {
	var err error
	// 首先判断是否是APK是就返回结果不是拉到不在计算其余部分
	apkName, pkg, version, issuer, subject, signMd5, signSha1, serialNumber, target, err = utils.GetApkZipInfo(file)
	if err != nil {
		return
	}
	// 获取md5
	apkMd5, err = utils.ApkMd5(file)
	if err != nil {
		log.Fatalf("utils.ApkMd5:", err)
		return
	}
	// 文件大小
	size, err = utils.ApkSize(file)
	if err != nil {
		log.Printf("utils.ApkSize:", err)
		return
	}

	// 是否启用杀毒 默认0 不启用
	if tx == 0 {
	} else {
		xcheck, desc = network.TXcheck(apkMd5)
	}
	if vt == 0 {
	} else {
		// 获取配置文件启用在线杀毒
		conf, err := utils.GetConf("vt.ini", "VT", "USER_KEY")
		if err != nil {
			log.Fatalf("utils.GetConf:%v", err)
		}else {
			split := strings.Split(conf, ";")
			rand.Seed(time.Now().UnixNano())
			index := rand.Intn(len(split)) //随机分配一个
			qihoo, baidu, rising, kaspers, alibaba, kingsoft, tencent, esetnod, jiangmin, vtRet = network.VTResultV3(split[index], apkMd5)
		}
	}
	return
}
