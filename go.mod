module ApkInfo

go 1.16

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	github.com/Unknwon/goconfig v0.0.0-20200908083735-df7de6a44db8
	// github.com/shogo82148/androidbinary v1.0.2
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.2.3-0.20181224173747-660f15d67dbb
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
)
