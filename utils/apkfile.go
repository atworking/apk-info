/**
* @Date:    2021/4/20 21:56
* @Author:  韩星星
* @Description: apk文件相关
 */

package utils

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"os"
	"strings"
	"github.com/Unknwon/goconfig"
)

// 获取文件大小
func ApkSize(path string) (int64, error) {
	f, err := os.Stat(path)
	if err != nil {
		return 0, err
	}
	return f.Size(), nil
}

// 计算文件md5
func ApkMd5(path string) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	md5h := md5.New()
	_, _ = io.Copy(md5h, f)
	result := hex.EncodeToString(md5h.Sum(nil))
	return strings.ToUpper(result), nil
}

// 获取配置文件ini
func GetConf(file, section, key string) (string, error) {
	cfg, err := goconfig.LoadConfigFile(file)
	if err != nil {
		return "", err
		//return "1489c03775ba366dbcb8c50c06943def4418da771cb3bf6317ac46bee697b4d5"
	}
	value, err := cfg.GetValue(section, key)
	if err != nil {
		return "", err
		//return "1489c03775ba366dbcb8c50c06943def4418da771cb3bf6317ac46bee697b4d5"
	}
	return value, nil
}
