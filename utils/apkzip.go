/**
* @Date:    2021/4/20 21:26
* @Author:  韩星星
* @Description:  获取APK ZIP包信息
 */

package utils

import (
	"ApkInfo/lib/androidbinary-1.0.2"
	"ApkInfo/lib/androidbinary-1.0.2/apk"
	"ApkInfo/sign"
	"archive/zip"
	"log"
)

/**
 * @Description		通过解压缩包获取的信息
 * @param file  	apk文件
 * @return apkName  	应用名
 * @return pkg  	包名
 * @return version	版本号
 * @return issuer	签名issuer
 * @return subject	签名subject
 * @return signMd5	签名md5
 * @return signSha1	签名sha1
 * @return serialNumber	签名序列号
 * @return target	TargetSDK版本
 */

func GetApkZipInfo(file string) (apkName, pkg, version, issuer, subject, signMd5, signSha1, serialNumber string, target int32, err error) {
	openFile, err := apk.OpenFile(file)
	if err != nil {
		if err == zip.ErrFormat {
			log.Printf("[%s]文件不是APK", file)
			return
		}
		return
	}
	resConfigEN := &androidbinary.ResTableConfig{
		Language: [2]uint8{uint8('e'), uint8('n')},
	}
	apkName, err = openFile.Label(resConfigEN) //应用名
	if err != nil {
		apkName = ""
	}
	pkg = openFile.PackageName()                           // 包名
	version = openFile.Manifest().VersionName.MustString() // 版本
	target = openFile.Manifest().SDK.Target.MustInt32()

	// 签名文件  ReadSignerFile 自己实现的方法在源码中apk中 具体实现方法见下注释
	b, err := openFile.ReadSignerFile()
	if err != nil {
		log.Println("获取签名文件失败：", err)
		err = nil
		return
	}
	issuer, subject, serialNumber, signMd5, signSha1, err = sign.APKSign(b)
	if err != nil {
		err = nil
		return
	}
	return
}

/*
// 增加补充方法获取签名 在源码 apk.go实现 和结构体APK一个文件
func (k *Apk) ReadSignerFile() (data []byte, err error) {
	buf := bytes.NewBuffer(nil)
	for _, file := range k.zipreader.File {
		if !strings.Contains(file.Name,"META-INF/"){
			continue
		}
		if !strings.HasSuffix(file.Name, ".RSA") && !strings.HasSuffix(file.Name,".DSA"){
			continue
		}
		rc, er := file.Open()
		if er != nil {
			err = er
			return
		}
		defer rc.Close()
		_, err = io.Copy(buf, rc)
		if err != nil {
			return
		}
		return buf.Bytes(), nil
	}
	return nil, fmt.Errorf("File %s not found", strconv.Quote("签名文件"))
}
*/
