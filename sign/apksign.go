/**
* @Date:    2021/4/22 10:50
* @Author:  韩星星
* @Description: 通过字节计算APK签名
 */

package sign

import (
	"bytes"
	"crypto/md5"
	"crypto/sha1"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"math/big"
	"strings"
)

var (
	OIDSignedData             = asn1.ObjectIdentifier{1, 2, 840, 113549, 1, 7, 2}
	ErrUnsupportedContentType = errors.New("pkcs7: cannot parse data: unimplemented content type")
)

var encodeIndent = 0

type issuerAndSerial struct {
	IssuerName   asn1.RawValue
	SerialNumber *big.Int
}

type attribute struct {
	Type  asn1.ObjectIdentifier
	Value asn1.RawValue `asn1:"set"`
}

type signerInfo struct {
	Version                   int `asn1:"default:1"`
	IssuerAndSerialNumber     issuerAndSerial
	DigestAlgorithm           pkix.AlgorithmIdentifier
	AuthenticatedAttributes   []attribute `asn1:"optional,omitempty,tag:0"`
	DigestEncryptionAlgorithm pkix.AlgorithmIdentifier
	EncryptedDigest           []byte
	UnauthenticatedAttributes []attribute `asn1:"optional,omitempty,tag:1"`
}

type PKCS7 struct {
	Certificates *x509.Certificate
	raw          interface{}
}

// 不进行验证 有证书就返回获取签名
func (p7 *PKCS7) GetOnlySigner() *x509.Certificate {
	return p7.Certificates
}

/* 以下是解析签名文件 */

type asn1Object interface {
	EncodeTo(writer *bytes.Buffer) error
}

func debugprint(format string, a ...interface{}) {
}

type contentInfo struct {
	ContentType asn1.ObjectIdentifier
	Content     asn1.RawValue `asn1:"explicit,optional,tag:0"`
}

type asn1Primitive struct {
	tagBytes []byte
	length   int
	content  []byte
}
type asn1Structured struct {
	tagBytes []byte
	content  []asn1Object
}
type rawCertificates struct {
	Raw asn1.RawContent
}

type signedData struct {
	Version                    int                        `asn1:"default:1"`
	DigestAlgorithmIdentifiers []pkix.AlgorithmIdentifier `asn1:"set"`
	ContentInfo                contentInfo
	Certificates               rawCertificates        `asn1:"optional,tag:0"`
	CRLs                       []pkix.CertificateList `asn1:"optional,tag:1"`
	SignerInfos                []signerInfo           `asn1:"set"`
}

func (p asn1Primitive) EncodeTo(out *bytes.Buffer) error {
	_, err := out.Write(p.tagBytes)
	if err != nil {
		return err
	}
	if err = encodeLength(out, p.length); err != nil {
		return err
	}
	out.Write(p.content)
	return nil
}

func lengthLength(i int) (numBytes int) {
	numBytes = 1
	for i > 255 {
		numBytes++
		i >>= 8
	}
	return
}
func marshalLongLength(out *bytes.Buffer, i int) (err error) {
	n := lengthLength(i)

	for ; n > 0; n-- {
		err = out.WriteByte(byte(i >> uint((n-1)*8)))
		if err != nil {
			return
		}
	}

	return nil
}

func encodeLength(out *bytes.Buffer, length int) (err error) {
	if length >= 128 {
		l := lengthLength(length)
		err = out.WriteByte(0x80 | byte(l))
		if err != nil {
			return
		}
		err = marshalLongLength(out, length)
		if err != nil {
			return
		}
	} else {
		err = out.WriteByte(byte(length))
		if err != nil {
			return
		}
	}
	return
}

func (s asn1Structured) EncodeTo(out *bytes.Buffer) error {
	encodeIndent++
	inner := new(bytes.Buffer)
	for _, obj := range s.content {
		err := obj.EncodeTo(inner)
		if err != nil {
			return err
		}
	}
	encodeIndent--
	out.Write(s.tagBytes)
	encodeLength(out, inner.Len())
	out.Write(inner.Bytes())
	return nil
}

func readObject(ber []byte, offset int) (asn1Object, int, error) {
	berLen := len(ber)
	if offset >= berLen {
		return nil, 0, errors.New("ber2der: offset is after end of ber data")
	}
	tagStart := offset
	b := ber[offset]
	offset++
	if offset >= berLen {
		return nil, 0, errors.New("ber2der: cannot move offset forward, end of ber data reached")
	}
	tag := b & 0x1F // last 5 bits
	if tag == 0x1F {
		tag = 0
		for ber[offset] >= 0x80 {
			tag = tag*128 + ber[offset] - 0x80
			offset++
			if offset > berLen {
				return nil, 0, errors.New("ber2der: cannot move offset forward, end of ber data reached")
			}
		}
		offset++
		if offset > berLen {
			return nil, 0, errors.New("ber2der: cannot move offset forward, end of ber data reached")
		}
	}
	tagEnd := offset

	kind := b & 0x20
	if kind == 0 {
		debugprint("--> Primitive\n")
	} else {
		debugprint("--> Constructed\n")
	}
	var length int
	l := ber[offset]
	offset++
	if offset > berLen {
		return nil, 0, errors.New("ber2der: cannot move offset forward, end of ber data reached")
	}
	hack := 0
	if l > 0x80 {
		numberOfBytes := (int)(l & 0x7F)
		if numberOfBytes > 4 { // int is only guaranteed to be 32bit
			return nil, 0, errors.New("ber2der: BER tag length too long")
		}
		if numberOfBytes == 4 && (int)(ber[offset]) > 0x7F {
			return nil, 0, errors.New("ber2der: BER tag length is negative")
		}
		// 更新如果为签名部分信息为空解析错误
		//if (int)(ber[offset]) == 0x0 {
		//	return nil, 0, errors.New("ber2der: BER tag length has leading zero")
		//}
		debugprint("--> (compute length) indicator byte: %x\n", l)
		debugprint("--> (compute length) length bytes: % X\n", ber[offset:offset+numberOfBytes])
		for i := 0; i < numberOfBytes; i++ {
			length = length*256 + (int)(ber[offset])
			offset++
			if offset > berLen {
				return nil, 0, errors.New("ber2der: cannot move offset forward, end of ber data reached")
			}
		}
	} else if l == 0x80 {
		markerIndex := bytes.LastIndex(ber[offset:], []byte{0x0, 0x0})
		if markerIndex == -1 {
			return nil, 0, errors.New("ber2der: Invalid BER format")
		}
		length = markerIndex
		hack = 2
		debugprint("--> (compute length) marker found at offset: %d\n", markerIndex+offset)
	} else {
		length = (int)(l)
	}
	if length < 0 {
		return nil, 0, errors.New("ber2der: invalid negative value found in BER tag length")
	}
	contentEnd := offset + length
	if contentEnd > len(ber) {
		return nil, 0, errors.New("ber2der: BER tag length is more than available data")
	}
	debugprint("--> content start : %d\n", offset)
	debugprint("--> content end   : %d\n", contentEnd)
	debugprint("--> content       : % X\n", ber[offset:contentEnd])
	var obj asn1Object
	if kind == 0 {
		obj = asn1Primitive{
			tagBytes: ber[tagStart:tagEnd],
			length:   length,
			content:  ber[offset:contentEnd],
		}
	} else {
		var subObjects []asn1Object
		for offset < contentEnd {
			var subObj asn1Object
			var err error
			subObj, offset, err = readObject(ber[:contentEnd], offset)
			if err != nil {
				return nil, 0, err
			}
			subObjects = append(subObjects, subObj)
		}
		obj = asn1Structured{
			tagBytes: ber[tagStart:tagEnd],
			content:  subObjects,
		}
	}
	return obj, contentEnd + hack, nil
}

func ber2der(ber []byte) ([]byte, error) {
	if len(ber) == 0 {
		return nil, errors.New("ber2der: input ber is empty")
	}
	out := new(bytes.Buffer)

	obj, _, err := readObject(ber, 0)
	if err != nil {
		return nil, err
	}
	obj.EncodeTo(out)
	return out.Bytes(), nil
}

func (raw rawCertificates) rawParse() ([]*x509.Certificate, error) {
	if len(raw.Raw) == 0 {
		return nil, nil
	}
	var val asn1.RawValue
	if _, err := asn1.Unmarshal(raw.Raw, &val); err != nil {
		return nil, err
	}
	return x509.ParseCertificates(val.Bytes)
}

func parseSignedData(data []byte) (*PKCS7, error) {
	var sd signedData
	asn1.Unmarshal(data, &sd)
	certs, err := sd.Certificates.rawParse()
	if err != nil {
		return nil, err
	}
	if len(certs) == 1 {
		return &PKCS7{
			Certificates: certs[0],
			raw:          sd,
		}, nil
	}
	return nil, errors.New("无法解析签名")
}

func Parse(data []byte) (p7 *PKCS7, err error) {
	if len(data) == 0 {
		return nil, errors.New("pkcs7: input data is empty")
	}
	var info contentInfo
	der, err := ber2der(data)
	if err != nil {
		return nil, err
	}
	rest, err := asn1.Unmarshal(der, &info)
	if len(rest) > 0 {
		err = asn1.SyntaxError{Msg: "trailing data"}
		return
	}
	if err != nil {
		return
	}
	if info.ContentType.Equal(OIDSignedData) {
		return parseSignedData(info.Content.Bytes)
	}
	return nil, ErrUnsupportedContentType
}

/**
 * @Description: 	返回APK签名
 * @param b 		：参数：字节类型的CENTER.RSA
 * @return issuer	：返回：发布者
 * @return subject	：返回: 持有者
 * @return serialNumber :返回： 序列号
 * @return signMd5	:返回：指纹md5
 * @return signSha1	:返回：指纹sha1
 * @return err		:返回：出错信息
 */

func APKSign(b []byte) (issuer, subject, serialNumber, signMd5, signSha1 string, err error) {
	pkcs7, err := Parse(b)
	if err != nil {
		log.Printf("解析签名失败:%v", err)
		return
	}
	/*出现 runtime error: invalid memory address or nil pointer dereference 错误
	修改源代码 因为有可能证书修改了和发行人不一致，这里修改无论任何证书都返回*/
	signer := pkcs7.GetOnlySigner()
	issuer = signer.Issuer.String()
	subject = signer.Subject.String()

	num := fmt.Sprintf("%X", signer.SerialNumber)
	if len(num) == 1 || len(num) == 7 {
		num = "0" + num
	}
	serialNumber = num
	raw := signer.Raw //证书的所有信息
	// 计算 MD5
	m := md5.Sum(raw)
	signMd5 = strings.ToUpper(hex.EncodeToString(m[:])) //转为大写
	s := sha1.Sum(raw)
	signSha1 = strings.ToUpper(hex.EncodeToString(s[:]))
	return
}
