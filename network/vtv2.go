/**
* @Date:    2021/4/20 22:11
* @Author:  韩星星
* @Description: VT在线v2版本
 */

package network

import (
	"io/ioutil"
	"net/http"
)

func VTResultV2(key, md5 string) string {
	url := "https://www.virustotal.com/vtapi/v2/file/report?apikey=" + key + "&resource=" + md5
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "0"
	}
	res, _ := http.DefaultClient.Do(req)
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	return string(body)
}