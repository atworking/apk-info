/**
* @Date:    2021/4/20 22:12
* @Author:  韩星星
* @Description: VT 在线v3版本
 */

package network

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
)

// v3 api版本错误返回
type ErrInfo struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}
type VTErr struct {
	VTerr ErrInfo `json:"error"`
}

// v3API正确的返回结果
type Qihoo struct {
	// 360
	Qihoo string `json:"result"`
}
type Baidu struct {
	Baidu string `json:"result"`
}
type Rising struct {
	Rising string `json:"result"`
}
type Kaspersky struct {
	Kaspersky string `json:"result"`
}
type Alibaba struct {
	Alibaba string `json:"result"`
}
type Kingsoft struct {
	Kingsoft string `json:"result"`
}
type Tencent struct {
	Tencent string `json:"result"`
}

type EsetNod struct {
	EsetNod string `json:"result"`
}
type Jiangmin struct {
	Jiangmin string `json:"result"`
}
type AnalysisResults struct {
	Qihoo     Qihoo     `json:"Qihoo-360"`
	Baidu     Baidu     `json:"Baidu"`
	Rising    Rising    `json:"Rising"`
	Kaspersky Kaspersky `json:"Kaspersky"`
	Alibaba   Alibaba   `json:"Alibaba"`
	Kingsoft  Kingsoft  `json:"Kingsoft"`
	Tencent   Tencent   `json:"Tencent"`
	EsetNod   EsetNod   `json:"ESET-NOD32"`
	Jiangmin  Jiangmin  `json:"Jiangmin"`
}

type AnalysisStats struct {
	Malicious  int `json:"malicious"`
	Undetected int `json:"undetected"`
}

type Analysis struct {
	Stats  *AnalysisStats   `json:"last_analysis_stats"`
	Result *AnalysisResults `json:"last_analysis_results"`
}

type VAttributes struct {
	VA *Analysis `json:"attributes"`
}
type VData struct {
	Data *VAttributes `json:"data"`
}

// 判断结构体是否为空
func (vtErr *VTErr) IsNotEmpty() bool {
	return reflect.DeepEqual(vtErr, &VTErr{})
}

// 使用V3版本
func VTResultV3(key, md5 string) (qihoo, baidu,rising,kaspers,alibaba,kingsoft,tencent,esetnod,jiangmin, result string) {
	var vtE *VTErr
	var vtData *VData
	// v3 api
	url := "https://www.virustotal.com/api/v3/files/" + md5
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("x-apikey", key)
	if err != nil {
		fmt.Println("http.NewRequest:",err)
		return
	}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("client.Do:",err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("ioutil.ReadAll:",err)
		return
	}
	err = json.Unmarshal(body, &vtE)
	if err != nil {
		fmt.Println("json.Unmarshal:",err)
		return
	}
	if vtE.IsNotEmpty() {
		err = json.Unmarshal(body, &vtData)
		if err != nil {
			fmt.Println("json.Unmarshal2:",err)
			return
		}
		// qihoo,baidu,rising,kaspers,alibaba,kingsoft,tencent,esetnod,jiangmin,
		qihoo = vtData.Data.VA.Result.Qihoo.Qihoo
		baidu = vtData.Data.VA.Result.Baidu.Baidu
		rising = vtData.Data.VA.Result.Rising.Rising
		kaspers = vtData.Data.VA.Result.Kaspersky.Kaspersky
		alibaba = vtData.Data.VA.Result.Alibaba.Alibaba
		kingsoft = vtData.Data.VA.Result.Kingsoft.Kingsoft
		tencent = vtData.Data.VA.Result.Tencent.Tencent
		esetnod = vtData.Data.VA.Result.EsetNod.EsetNod
		jiangmin= vtData.Data.VA.Result.Jiangmin.Jiangmin

		total := vtData.Data.VA.Stats.Malicious + vtData.Data.VA.Stats.Undetected
		result = strconv.Itoa(vtData.Data.VA.Stats.Malicious) + "/" + strconv.Itoa(total)
	}else {
		result = vtE.VTerr.Code
	}
	return
}
