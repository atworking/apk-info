/**
* @Date:    2021/4/20 22:07
* @Author:  韩星星
* @Description: 腾讯查杀
 */

package network

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type FCheck struct {
	Result string `json:"result"`
	Msg    string `json:"msg"`
}
type info struct {
	Platform   string `json:"platform"`
	SoftName   string `json:"softName"`
	VirusName  string `json:"virusName"`
	VirusDesc  string `json:"virusDesc"`
	Conclusion string `json:"conclusion"`
}
type TResult struct {
	Result string `json:"result"`
	Msg    string `json:"msg"`
	Info   info   `json:"info"`
}

func TXcheck(fMd5 string) (vName, vDesc string) {
	var info TResult
	url := "https://m.qq.com/security_lab/check_result_json.jsp?type=md5&data=" + fMd5
	resp2, err2 := http.Get(url)
	if err2 != nil {
		return "失败", "获取检测信息失败，请重试"
	}
	defer resp2.Body.Close()
	body2, err2 := ioutil.ReadAll(resp2.Body)
	if err2 != nil {
		return "失败", "腾讯在线未查找到匹配内容"
	}
	err := json.Unmarshal(body2, &info)
	if err != nil {
		return "失败", "未能解析对应的数据"
	}
	if info.Info.Conclusion == "未发现风险，可放心使用" {
		return "安全应用", "未发现风险，可放心使用"
	} else if info.Info.Conclusion == "未知软件，请稍后再试" {
		return "未知", "未知软件，请稍后再试"
	} else {
		return info.Info.VirusName, info.Info.VirusDesc
	}
}
